// Чому для роботи з input не рекомендується використовувати клавіатуру?
// На современных устройствах есть и другие способы «ввести что-то». Например, распознавание речи (это особенно актуально на мобильных устройствах) или Копировать/Вставить с помощью мыши.

// Поэтому, если мы хотим корректно отслеживать ввод в поле <input>, то одних клавиатурных событий недостаточно. 
// Реалізувати функцію підсвічування клавіш. Завдання має бути виконане на чистому Javascript без використання бібліотек типу jQuery або React.

// Технічні вимоги:

// У файлі index.html лежить розмітка для кнопок.
// Кожна кнопка містить назву клавіші на клавіатурі
// Після натискання вказаних клавіш - та кнопка, на якій написана ця літера, повинна фарбуватися в синій колір. При цьому якщо якась інша літера вже раніше була пофарбована в синій колір - вона стає чорною. Наприклад за натисканням Enter перша кнопка забарвлюється у синій колір. Далі, користувач натискає S, і кнопка S забарвлюється в синій колір, а кнопка Enter знову стає чорною.

let btns = document.querySelectorAll(".btn");
console.log(btns);

document.addEventListener("keydown", keydown);

function keydown(event) {
    for (let item = 0; item < btns.length; item++) {
        
        if (btns[item].innerHTML === event.key || "Key" + btns[item].innerHTML === event.code) {
            btns[item].style.backgroundColor = "blue";
        } else (btns[item].style.backgroundColor = "black")
        
            }
        }
    